package com.example.provider.nsf;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/nsf/fuse")
public class Fuse {

    private static int rate = 0;

    @GetMapping("errorrate")
    public String errorRate()throws Exception{
        if(rate % 2 == 0){
            throw new Exception("An unexcepted error eccured from provider!");
        }else{
            return "Response from provider correctly!";
        }
    }

    @GetMapping("sleep")
    public String sleep()throws InterruptedException{
        Thread.sleep(2000);
        return "Response from provider after sleep 2 seconds!";
    }

}
