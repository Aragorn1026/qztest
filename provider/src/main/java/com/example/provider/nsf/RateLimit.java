package com.example.provider.nsf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

@RestController
@RequestMapping("/api/nsf/ratelimit")
public class RateLimit {
    public static int times = 0;

    @Autowired
    private HttpServletRequest request;

    @Value("${server.port}")
    private String port;

    @Value("${server.version}")
    private String version;

    @GetMapping("/query")
    public String query()throws Exception{
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.CHINA);
        DateFormat timeFormat = DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.CHINA);
        String time = dateFormat.format(new Date()) + " " + timeFormat.format(new Date());

        times ++;
//        System.out.println( time + request.getServerName() + ":" + request.getServerPort() );

        return time + " times:" + (times - 1) + " - response from provider: " + request.getServerName() + ":" +
                request.getServerPort() + " - with version: " + version + "\r\n";

    }

}
