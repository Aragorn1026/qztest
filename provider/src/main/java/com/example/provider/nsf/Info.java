package com.example.provider.nsf;
import org.omg.CORBA.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;


@RestController
@RequestMapping("/api/nsf/info")
public class Info {

    @Autowired
    private HttpServletRequest request;

    @Value("${server.port}")
    private String port;

    @Value("${server.version}")
    private String version;

    @GetMapping("instance")
    public String getInstanceInfo(@RequestParam(value = "anything", defaultValue = "") String anything) {
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.CHINA);
        DateFormat timeFormat = DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.CHINA);
        String time = dateFormat.format(new Date()) + " " + timeFormat.format(new Date());

        return time + " -- " + "response from provider: " + request.getServerName() + ":" + request.getServerPort()
                + " - with version: " + version + " " + anything;

    }

}
