package com.example.qztest.consumer.nsf;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

@Controller
@RequestMapping("/api/nsf/test/")
public class HTTPTest {

    @GetMapping("httpget")
    public void httptest(){
        String result = null;
        try{
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
            Map<String, Object> param = new HashMap<>();
            param.put("bizAppId","OA");
            param.put("bizTest","zcq");

            HttpEntity<Map<String, Object>> request = new HttpEntity<Map<String, Object>>(param, headers);

            String url = "http://localhost:8585/api/nsf/info/showjson";
            System.out.print("request url: " + url);

            RestTemplate taskRestTemplate = new RestTemplate();
            ResponseEntity<String> rs = taskRestTemplate.postForEntity(url, request, String.class);
            System.out.print("rs1-结束，返回结果是: " + rs.getBody());

            HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
            factory.setConnectionRequestTimeout(60 * 1000);
            factory.setConnectTimeout(60 * 1000);
            factory.setReadTimeout(100 * 1000);
            RestTemplate taskTemplate2 = new RestTemplate(factory);
            rs = taskTemplate2.postForEntity(url, request, String.class);
            System.out.print("rs2-结束，返回结果是: " + rs.getBody());

        }catch(Exception exception){
            System.out.print("有异常，终止访问: " + exception.toString());
        }
    }

}


































