package com.example.qztest.consumer.nsf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/nsf/fail")
public class Fail {
    public static int number = 0;

    @Autowired
    private HttpServletRequest request;

    @Value("${server.port}")
    private String port;

    @Value("${server.version}")
    private String version;

    @GetMapping("/failover")
    public ResponseEntity<Map<String,Object>> failover()throws Exception{
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.CHINA);
        DateFormat timeFormat = DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.CHINA);
        String time = dateFormat.format(new Date()) + " " + timeFormat.format(new Date());

        Map<String,Object> map = new HashMap<String,Object>();
        map.put("Time", time);
        map.put("message","Consumer is unreachable\r\n");
        map.put("code", HttpStatus.SERVICE_UNAVAILABLE.value());

        Thread.sleep(2000);

        return new ResponseEntity<Map<String,Object>>(map, HttpStatus.SERVICE_UNAVAILABLE);

    }

    @GetMapping("/failfast")
    public String failfast(){
        return "Consumer throws an exception quickly\r\n";
    }

    @GetMapping("/failback")
    public String failback()throws Exception{
        if(number % 5 == 0){
            number ++;
            throw new Exception("The value of number is illegal\r\n");
        }else{
            number ++;
            DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.CHINA);
            DateFormat timeFormat = DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.CHINA);
            String time = dateFormat.format(new Date()) + " " + timeFormat.format(new Date());

            System.out.println( time + request.getServerName() + ":" + request.getServerPort() );
            return time + " number:" + (number - 1) + " - response from consumer: " + request.getServerName() + ":" +
                    request.getServerPort() + " -  with version " + version + "\r\n";
        }
    }


}