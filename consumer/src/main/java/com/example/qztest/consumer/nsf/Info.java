package com.example.qztest.consumer.nsf;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
//import org.springframework.web.servlet.mvc.method.annotation.AbstractJsonpResponseBodyAdvice;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.util.*;
import io.swagger.annotations.ApiResponse;

@Api("Swagger2 zcqtest for Info")
@RestController
@RequestMapping("/api/nsf/info")
public class Info extends Jsonp {

    public static int index = 1;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    RestTemplate restTemplate;

    @Value("${server.port}")
    private String port;

    @Value("${server.version}")
    private String version;

    @Value("${server.appname}")
    private String appname;

    @GetMapping("instance")
    public String getInstanceInfo() {

        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.CHINA);
        DateFormat timeFormat = DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.CHINA);
        String time = dateFormat.format(new Date()) + " " + timeFormat.format(new Date());
        index ++;

        String headers = "Header infos from requests: \r\n";
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            String value = request.getHeader(key);

            headers += key + " --: " + value + "\r\n";
        }

        return time + " -- " + "from consumer: " + appname + " -- " + request.getServerName() + ":" + request.getServerPort() +
                " - with version: " + version + " ++ index:" + (index - 1) + "\r\n" + headers + "\r\n";

    }

    @CrossOrigin(origins = "http://www.baidu.com")
    @GetMapping("corstest")
    public String corstest() {

        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.CHINA);
        DateFormat timeFormat = DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.CHINA);
        String time = dateFormat.format(new Date()) + " " + timeFormat.format(new Date());
        index ++;

        String headers = "Header infos from cors test requests: \r\n";
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            String value = request.getHeader(key);

            headers += key + " --: " + value + "\r\n";
        }

        return time + " -- " + "from consumer: " + request.getServerName() + ":" + request.getServerPort() +
                " - with version: " + version + " ++ index:" + (index - 1) + "\r\n" + headers + "\r\n";

    }

    @GetMapping("queryproviders")
    public String queryProviders(@RequestParam(value = "sth", defaultValue = "") String sth) {
        String url = "http://qztestprovider/api/nsf/info/instance?anything=" + sth;

        return String.format(restTemplate.getForObject(url, String.class) + "\r\n");
    }

    @ApiOperation("传入指定参数的方法")
    @ApiImplicitParam("queryString部分sth可为任意对象")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = String.class),
            @ApiResponse(code = 404, message = "Lost way", response = String.class)})
    @RequestMapping("getquery")
    public String getQuery(@RequestParam(value = "sth", defaultValue = "") Object sth) {

        return String.format(sth.toString() + "\r\n");
    }


    @PostMapping("showbody")
    public String showbody(@RequestParam Map<String,String> map){
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.CHINA);
        DateFormat timeFormat = DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.CHINA);
        String time = dateFormat.format(new Date()) + " " + timeFormat.format(new Date());


//        @RequestBody Map<String,String> map
        String smap = time + "\n";
        for (Map.Entry<String, String> entry : map.entrySet()) {
            smap += "Got showbody POST body : Key = " + entry.getKey() + ", Value = " + entry.getValue() + "\n";
        }

        System.out.print(smap);
        return smap + "\r\n" + map.toString();

//        指定post请求传参名
//        @RequestParam(name = "name",defaultValue = "xxx") String name,@RequestParam( name = "age", required = false) Integer age
//        return "Thers is a person: " + name + ", age: " + age;

    }

//    @ResponseBody
//    @PostMapping(value = "showjson", produces = "application/json;charset=UTF-8")
//    public String showjson(@RequestBody JSONObject jsonParam){
//        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.CHINA);
//        DateFormat timeFormat = DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.CHINA);
//        String time = dateFormat.format(new Date()) + " " + timeFormat.format(new Date());
//
//        String sjson = time + "\n" + "POST json is: \n";
//        sjson += jsonParam.toJSONString();
//
//        return sjson;
//    }


    @PostMapping(value = "showjson")
    public JSON showjson(@RequestParam Map<String,String> map){
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.CHINA);
        DateFormat timeFormat = DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.CHINA);
        String time = dateFormat.format(new Date()) + " " + timeFormat.format(new Date());


//        @RequestBody Map<String,String> map
        String smap = time + "\n";
        for (Map.Entry<String, String> entry : map.entrySet()) {
            smap += "Got showjson POST body : Key = " + entry.getKey() + ", Value = " + entry.getValue() + "\n";
        }

        System.out.print(smap);

        JSON json = (JSON) JSON.toJSON(map);

        return json;
    }

    @GetMapping(value = "getjson")
    public JSON getjson(){
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.CHINA);
        DateFormat timeFormat = DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.CHINA);
        String time = dateFormat.format(new Date()) + " " + timeFormat.format(new Date());

        JSONObject json = new JSONObject();
        json.put("time", time);
        json.put("name", "zhangsan");
        json.put("age", index);
        index ++;

        return json;
    }



}