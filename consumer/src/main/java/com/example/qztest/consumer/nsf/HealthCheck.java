package com.example.qztest.consumer.nsf;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@RestController
@RequestMapping("/api/nsf/healthcheck")
public class HealthCheck {

    @GetMapping("/ok")
    public ResponseEntity<Map<String,Object>> ok() throws IOException {
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.CHINA);
        DateFormat timeFormat = DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.CHINA);
        String time = dateFormat.format(new Date()) + " " + timeFormat.format(new Date());

        Map<String,Object> map = new HashMap<String,Object>();
        map.put("code", HttpStatus.OK.value());
        map.put("Time", time);

        return new ResponseEntity<Map<String,Object>>(map, HttpStatus.OK);
    }

    @GetMapping("/nok")
    public ResponseEntity<Map<String,Object>> nok() throws IOException, InterruptedException {
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.CHINA);
        DateFormat timeFormat = DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.CHINA);
        String time = dateFormat.format(new Date()) + " " + timeFormat.format(new Date());

        Map<String,Object> map = new HashMap<String,Object>();
        map.put("Time", time);
        map.put("message","Consumer is unavailable");
        map.put("code", HttpStatus.SERVICE_UNAVAILABLE.value());

        Thread.sleep(2000);

        return new ResponseEntity<Map<String,Object>>(map, HttpStatus.SERVICE_UNAVAILABLE);
    }

}
