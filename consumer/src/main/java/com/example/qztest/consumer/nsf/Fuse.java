package com.example.qztest.consumer.nsf;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/nsf/fuse")
public class Fuse {

    private static int rate = 0;

    @GetMapping("errorrate")
    public String errorRate()throws Exception{
        rate ++;
        if(rate % 3 == 0){
            return "Response from consumer correctly!";
        }else if( rate % 3 == 1){
            Thread.sleep(2000);
            return "Response from consumer after sleep 2 seconds";
        }else{
            throw new Exception("An unexcepted error eccured from consumer!");
        }
    }

//    @GetMapping("errorrate")
//    public String errorRate()throws Exception{
//        rate ++;
//        if(rate % 2 == 0){
//            throw new Exception("An unexcepted error eccured from consumer!");
//        }else{
//            return "Response from consumer correctly!";
//        }
//    }

    @GetMapping("sleep")
    public String sleep()throws InterruptedException{
        Thread.sleep(2000);
        return "Response from consumer after sleep 2 seconds!";
    }

}