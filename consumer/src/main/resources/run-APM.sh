#!/bin/bash

cd /root/

java \
-javaagent:/root/nsf-agent-v2.6.8.jar=qztestconsumer-v1 \
-javaagent:/root/skywalking-napm-bin-8.0.0-latest/agent/skywalking-agent.jar \
-Dskywalking_config=/root/skywalking-napm-bin-8.0.0-latest/agent/config/agent-consumer.config \
-Dnsf.log.level=debug \
-jar qztestconsumer-v1.jar