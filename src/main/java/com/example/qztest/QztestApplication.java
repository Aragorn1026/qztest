package com.example.qztest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QztestApplication {

	public static void main(String[] args) {
		SpringApplication.run(QztestApplication.class, args);
	}

}
